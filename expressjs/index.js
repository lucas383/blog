const express = require('express')
const { Client } = require('pg')
const jwt = require('jsonwebtoken')
const cors = require('cors')
require('dotenv').config();

const app = express()
app.use(express.json())
app.use(cors())

const client = new Client({
    user: "lucas",
    host: "localhost",
    database: "blog",
    password: "Lucas!N3wDe4l",
    port: 5432
})
client.connect(function (err) {
    if (err) {
        console.error(err)
    }

    console.log("connected")
})

const name = "Lucas"
let email = "lucas@example.com"
let password = "an encrypted password"

// create user with encrypted password

// client.query('INSERT INTO user_account (user_name, email, password) VALUES ( $1, $2, crypt($3, gen_salt(\'bf\')) ) RETURNING *;', [name, email, password], (error, results) => {
//     if (error) {
//         throw error
//       }
//     console.log(results.rows[0]);
// })

app.post('/api/register', (req, res) => {
    const {email, username, password} = req.body
    if (!email || !username || !password) {
        return res.status(422).json({ message: 'Error. Please enter your email, username and password'})
    }

    if (password.length < 8) {
        return res.status(422).json({ message: 'Error. Password should be at least 8 characters long'})
    }

    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g
    if (!pattern.test(email)) {
        return res.status(422).json({ message: `Error. ${email} is not a valid email`})
    }

    let takenUsernames= []
    client.query('SELECT user_name from user_account;', (error, results) => {
        if (error) {
            return res.status(500).json({ error_code: error.code })
        }

        takenUsernames = results.rows
        if (takenUsernames.findIndex(e => e.user_name === username) !== -1) {
            return res.status(422).json({ message: `Error. username ${username} is taken`})
        }

        client.query('INSERT INTO user_account (user_name, email, password) VALUES($1, $2, crypt($3, gen_salt(\'bf\'))) RETURNING *', [username, email, password], (error, results) => {
            if (error) {
                return res.status(500).json({ error_code: error.code })
            }

            return res.status(201).json({ message: `User created `})
        })
    })

})


// login
app.post('/api/login', (req, res) => {
    const { username, password } = req.body

    if (!username || !password) {
        return res.status(400).json({ message: 'Error. Please enter your credentials' })
    }

    client.query('SELECT id, user_name from user_account WHERE user_name=$1 AND password=crypt($2, password);', [username, password], (error, results) => {
        if (error) {
            res.status(500).json({ error_code: error.code })
        } else {
            const user = results.rows[0]
            if (user) {
                const accessToken = jwt.sign({ ...user }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '3h' })

                res.json({ accessToken })
            } else {
                res.status(401).json({ message: 'Error. Incorrect credentials' })
            }
        }
    })
})

// validate JWT token
const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization

    if (authHeader) {
        const token = authHeader.split(' ')[1]

        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
            if(err) {
                return res.sendStatus(403)
            }

            req.user = user
            next()
        })

    } else {
        res.sendStatus(401)
    }
}

// Create a post
app.post('/api/post', authenticateJWT, (req, res) => {
    const user_id = req.user.id
    const { image_url, title, body } = req.body;

    client.query('INSERT INTO post (author_id, image_url, title, body) VALUES ($1, $2, $3, $4)', [user_id, image_url, title, body], (error, results) => {
        if (error) {
            res.status(500).json({ error_code: error.code })
        } else {
            res.sendStatus(201)
        }
    })
})

// get all posts
app.get('/api/posts', (req, res) => {
    client.query('SELECT * from post;', (error, results) => {
        if (error) {
            res.status(500).json({ error_code: error.code})
        } else {
            res.json(results.rows)
        }
    })
})

// get one post
app.get('/api/post/:id', (req, res) => {
    client.query('SELECT * from post where id = $1', [req.params.id], (error, results) => {
        if (error) {
            res.status(500).json({ error_code: error.code})
        } else {
            res.json(results.rows[0])
        }
    })
})


app.post('/api/me', authenticateJWT, (req, res) => {
    const authenticatedUserId = req.user.id
    client.query('SELECT * from user_account where id = $1', [authenticatedUserId], (error, results) => {
        const {password, ...restOfUser} = results.rows[0]
        res.json( { user: restOfUser } )
    })
})

// get all usernames
app.get('/api/usernames', (req, res) => {
    client.query('SELECT user_name from user_account;', (error, results) => {
        if (error) {
            res.status(500).json({ error_code: error.code })
        } else {
            res.json(results.rows)
        }
    })
})

app.listen(process.env.PORT, () => {
    console.log(`server is running on port ${process.env.PORT}`)
})